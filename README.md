# Fedimins-Website

[![Codeberg CI](https://ci.codeberg.org/api/badges/Tealk/website/status.svg)](https://ci.codeberg.org/Tealk/website)
![Framework](https://img.shields.io/badge/Framework-hugo-pink)
![Lizenz](https://img.shields.io/badge/Lizenz-CC--BY--NC--SA--4.0-orange)

Dieses Repository enthält mein Portfolio, das Informationen über meine Projekte, Arbeitsplätze und Fähigkeiten enthält.

## Inhaltsverzeichnis

- [Fedimins-Website](#fedimins-website)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Über das Portfolio](#über-das-portfolio)
  - [Installation](#installation)
  - [Verwendung](#verwendung)
  - [Kontakt](#kontakt)
  - [Lizenz](#lizenz)

## Über das Portfolio

Mein Portfolio ist eine Zusammenstellung meiner Arbeitserfahrung, Projekte und Fähigkeiten. Es bietet einen Überblick über meine berufliche Laufbahn, meine technischen Fähigkeiten und die Projekte, an denen ich gearbeitet habe. Das Portfolio ist in einer übersichtlichen und informativen Art und Weise gestaltet, um potenziellen Arbeitgebern oder Interessenten einen Einblick in meine Kompetenzen zu geben.

## Installation

Um die Fedimins-Website lokal auszuführen, führen Sie bitte die folgenden Schritte aus:

1. Stellen Sie sicher, dass Sie eine aktuelle Version von Hugo auf Ihrem System installiert haben.

2. Klonen Sie das Repository mit Git:
```
git clone https://codeberg.org/Tealk/website.git
```

3. Wechseln Sie in das Verzeichnis der Website:
```
cd website
```

4. Führen Sie den Befehl `hugo server` aus, um den Entwicklungsserver zu starten.

## Verwendung

Nachdem Sie den Entwicklungsserver gestartet haben, können Sie die Fedimins-Website in Ihrem Webbrowser unter der Adresse http://localhost:1313 anzeigen. Der Entwicklungsserver überwacht Änderungen an den Dateien und lädt die Website automatisch neu, wenn Sie Änderungen vornehmen.

## Kontakt

Für Fragen oder Anliegen können Sie mich über die Kontaktdaten auf meiner [Website](https://buck.zone/) erreichen.

## Lizenz

Dieses Portfolio ist unter der [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/) lizenziert.