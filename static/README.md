Portfolio
============
![Framework](https://img.shields.io/badge/Framework-hugo-pink) ![Lizenz](https://img.shields.io/badge/Lizenz-CC--BY--NC--SA--4.0-orange) [![SourceCode](https://img.shields.io/badge/SoruceCode-codeberg.org-green)](https://codeberg.org/Tealk/pages-source)

# Allgemein
Dies ist das Repository meiner Webseite.

## Erstellt mit
* [Hugo](https://gohugo.io/)
* [modifyed sada Theme](https://github.com/darshanbaral/sada)
* [Codeberg Pages](https://docs.codeberg.org/codeberg-pages/)